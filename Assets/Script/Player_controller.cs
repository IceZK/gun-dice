using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Player
{
    public class Player_controller : MonoBehaviour
    {
        // Start is called before the first frame update
        public Transform bullet_spawn1;
        public Transform bullet_spawn2;
        public GameObject bullet_l;
        public GameObject bullet_r;
        public Slider guage_bar;
        public SpriteRenderer sr;
        public Animator animator;
        public AudioSource audioSource;
        public int ammo = 6;
        public int health = 3;
        public int guage;
        public int max_guage;
        public int rcdtrigger = 1;
        public int dice;

        public float cd = 0;
        public float maxcd = 2;

        public bool isrcd = false;

        public GameObject gameover;
        void Start()
        {
            max_guage = 100;
            guage = 0;
            animator = GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        void Update()
        {
            cd -= Time.deltaTime;
            guage_bar.value = guage;
            guage_bar.maxValue = max_guage;


            if (ammo > 0)
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {

                    Instantiate(bullet_l, bullet_spawn1.transform.position, Quaternion.identity);
                    animator.SetTrigger("shoot");
                    audioSource.Play();
                    sr.flipX = true;
                    ammo--;
                }
                if (Input.GetKeyDown(KeyCode.RightArrow))

                {
                    Instantiate(bullet_r, bullet_spawn2.transform.position, Quaternion.identity);
                    animator.SetTrigger("shoot");
                    audioSource.Play();
                    sr.flipX = false;
                    ammo--;
                }
            }
            if (Input.GetKeyDown(KeyCode.Space) && cd <= 0 && ammo < 6)
            {
                Reload();
                //GuagePower(RollDice());
            }

            if(guage >= max_guage)
            {
                guage = 0;
                SpecialMove();
            }


            if (health <= 0)
            {
                Gameover();
            }


            if (Mathf.FloorToInt(GameManager.gm_instance.score / 25) == rcdtrigger && maxcd > 0.25f && !isrcd)    
            {
                maxcd -= 0.01f;
                isrcd = true;
                
            }
            if (Mathf.FloorToInt(GameManager.gm_instance.score / 25) < rcdtrigger)
            {
                isrcd = false;
            }
            rcdtrigger = Mathf.FloorToInt(GameManager.gm_instance.score / 25) + 1;



        }

        void Gameover()
        {
            SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
        }
        //lose health && combo
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "enemy")
            {

                Destroy(collision.gameObject);
                health -= 1;
                GameManager.gm_instance.combo_stack = 0;
            }
        }
        public void Reload()
        {
            //roll dice
            ammo = 6;
            cd = maxcd;
        }
        public int RollDice()
        {
            int i = Random.Range(1, 7);
            return i;

        }
        public void GuagePower(int dice)
        {
            guage += dice;
        }
        public void SpecialMove()
        {
            GameObject[] enemy;
            enemy = GameObject.FindGameObjectsWithTag("enemy");
            foreach (GameObject e in enemy)
            {
                GameManager.gm_instance.combo_stack += 1;
                Destroy(e.gameObject);
            }
        }

    }
}

