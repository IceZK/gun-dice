using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)) 
        {
            GameManager.gm_instance.score = 0;
            GameManager.gm_instance.combo_stack = 0;
            SceneManager.LoadScene("Gameplay", LoadSceneMode.Single);
        }
    }
}
