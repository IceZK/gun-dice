using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class DiceUI : MonoBehaviour
{
    // Start is called before the first frame update
    Animator animator;
    public GameObject image_dice1;
    public GameObject image_dice2;
    public GameObject image_dice3;
    public GameObject image_dice4;
    public GameObject image_dice5;
    public GameObject image_dice6;
    
    public int state;
    public float cd;
    public float maxcd;
   

    void Start()
    {
        animator = GetComponent<Animator>();
        maxcd = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        cd -= Time.deltaTime;

    }
    public void Roll()
    {
        
        animator.SetInteger("Dice", 0);
        animator.SetBool("Roll", true);
        image_dice1.SetActive(false);
        image_dice2.SetActive(false);
        image_dice3.SetActive(false);
        image_dice4.SetActive(false);
        image_dice5.SetActive(false);
        image_dice6.SetActive(false);
        state = 0;
    }
    public void D1()
    {
        image_dice1.SetActive(true);
        image_dice2.SetActive(false);
        image_dice3.SetActive(false);
        image_dice4.SetActive(false);
        image_dice5.SetActive(false);
        image_dice6.SetActive(false);
        
        /*
        animator.SetInteger("Dice", 1);
        animator.SetBool("Roll", false);
        */
        state = 1;
    }
    public void D2()
    {
        image_dice1.SetActive(false);
        image_dice2.SetActive(true);
        image_dice3.SetActive(false);
        image_dice4.SetActive(false);
        image_dice5.SetActive(false);
        image_dice6.SetActive(false);

        state = 2;
    }
    public void D3()
    {
        image_dice1.SetActive(false);
        image_dice2.SetActive(false);
        image_dice3.SetActive(true);
        image_dice4.SetActive(false);
        image_dice5.SetActive(false);
        image_dice6.SetActive(false);

        state = 3;
    }
    public void D4()
    {
        image_dice1.SetActive(false);
        image_dice2.SetActive(false);
        image_dice3.SetActive(false);
        image_dice4.SetActive(true);
        image_dice5.SetActive(false);
        image_dice6.SetActive(false);

        state = 4;
    }
    public void D5()
    {
        image_dice1.SetActive(false);
        image_dice2.SetActive(false);
        image_dice3.SetActive(false);
        image_dice4.SetActive(false);
        image_dice5.SetActive(true);
        image_dice6.SetActive(false);

        state = 5;
    }
    public void D6()
    {
        image_dice1.SetActive(false);
        image_dice2.SetActive(false);
        image_dice3.SetActive(false);
        image_dice4.SetActive(false);
        image_dice5.SetActive(false);
        image_dice6.SetActive(true);

        state = 6;
    }

    public void ChangeAnimation(int dice)
    {
        switch (dice)
        {
            case 1: D1(); break;
            case 2: D2(); break;
            case 3: D3(); break;
            case 4: D4(); break;
            case 5: D5(); break;
            case 6: D6(); break;
        }
        cd = maxcd;
        if (cd < 0) Roll();
    }
}
