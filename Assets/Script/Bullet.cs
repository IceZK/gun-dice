using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody2D rb;
    public int speed;
    public float die_time;
    public int dice = 0;
    public float cd;
    public float maxcd;
    public GameObject enemy;
    public AiMovement ai;
    public Player_controller pc;
    public DiceUI diceUI;
    public GameObject EFX;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        pc = GameObject.FindWithTag("Player").GetComponent<Player_controller>();
        diceUI = GameObject.FindWithTag("dice").GetComponent<DiceUI>();
        maxcd = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(speed, 0);
        Destroy(this.gameObject, die_time);
        cd -= Time.deltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemy")
        {
            Instantiate(EFX, this.gameObject.transform.position, Quaternion.identity);
            ai = collision.gameObject.GetComponent<AiMovement>();
            if (ai.dice)
            {
                dice = pc.RollDice();

                pc.GuagePower(dice);

                diceUI.ChangeAnimation(dice);
            }
            
            
            Destroy(collision.gameObject);
            GameManager.gm_instance.combo_stack += 1;
            GameManager.gm_instance.score += 1 + Mathf.FloorToInt(GameManager.gm_instance.combo_stack/10);
            if (GameManager.gm_instance.combo_stack % 25 == 0 && GameManager.gm_instance.combo_stack > 0)
            {
                GameManager.gm_instance.score += 100;
            }
           
            Destroy(this.gameObject);
        }
    }


}   
