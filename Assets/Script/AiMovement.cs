using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject player;
    Rigidbody2D rb;
    public float speed;
    public SpriteRenderer sr;
    public bool dice;


    void Start()
    {
        player = GameObject.FindWithTag("Player");
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (this.transform.position.x < player.transform. position.x)
        {
            rb.velocity = new Vector2(speed, 0);
            sr.flipX = true;
            
        }
        else if (this.transform.position.x > player.transform.position.x)
        {
            rb.velocity = new Vector2(-speed, 0);
            sr.flipX = false;
        }
    }
    
}
