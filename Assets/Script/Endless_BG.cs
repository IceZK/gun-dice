using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class Endless_BG : MonoBehaviour
{
    // Start is called before the first frame update
    [Range(1f,10f)]
    public float scrollspeed = 1.0f;
    private float offset;
    private SpriteRenderer sr;
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        sr.size += new Vector2(scrollspeed * Time.deltaTime, 0);
    }
}
