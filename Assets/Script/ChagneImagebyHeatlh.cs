using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;
using UnityEngine.UI;

public class ChagneImagebyHeatlh : MonoBehaviour
{
    // Start is called before the first frame update
    public Image player_image;
    

    public Sprite image1;
    public Sprite image2;
    public Sprite image3;

    public Player_controller pc;
    void Start()
    {
        
        pc = GameObject.FindWithTag("Player").GetComponent<Player_controller>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (pc.health)
        {
            case 3: player_image.sprite = image1; break;
            case 2: player_image.sprite = image2; break;
            case 1: player_image.sprite = image3; break;

        }

    }
}
