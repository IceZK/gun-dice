using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class Health : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject heart1;
    public GameObject heart2;
    public GameObject heart3;

    public int health;
    public Player_controller player_script;
   

    // Update is called once per frame
    void Update()
    {
        health = player_script.health;

        if (health == 3)
        {
            heart1.SetActive(true);
            heart2.SetActive(true);
            heart3.SetActive(true);
        }
        if (health == 2)
        {
            heart1.SetActive(true);
            heart2.SetActive(true);
            heart3.SetActive(false);
        }
        if (health == 1)
        {
            heart1.SetActive(true);
            heart2.SetActive(false);
            heart3.SetActive(false);
        }
        if (health == 0)
        {
            heart1.SetActive(false);
            heart2.SetActive(false);
            heart3.SetActive(false);
        }
    }
}
