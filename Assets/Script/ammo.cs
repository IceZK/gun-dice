using System.Collections;
using System.Collections.Generic;
using Player;
using TMPro;
using UnityEngine;

public class ammo : MonoBehaviour
{
    // Start is called before the first frame update
    public Player_controller sc_player;
    public int ammos;
    //public TextMeshProUGUI text_ammo;

    public GameObject image_ammo1;
    public GameObject image_ammo2;
    public GameObject image_ammo3;
    public GameObject image_ammo4;
    public GameObject image_ammo5;
    public GameObject image_ammo6;

    void Start()
    {
        sc_player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_controller>();

    }

    // Update is called once per frame
    void Update()
    {
        ammos = sc_player.ammo;
        //text_ammo.text = "AMMO " + ammos + "/6";
        switch (ammos) {
            case 6: image_ammo1.SetActive(true);
                    image_ammo2.SetActive(true);
                    image_ammo3.SetActive(true);
                    image_ammo4.SetActive(true);
                    image_ammo5.SetActive(true);
                    image_ammo6.SetActive(true);
                    break;
            case 5: image_ammo1.SetActive(true);
                    image_ammo2.SetActive(true);
                    image_ammo3.SetActive(true);
                    image_ammo4.SetActive(true);
                    image_ammo5.SetActive(true);
                    image_ammo6.SetActive(false);
                    break;
            case 4: image_ammo1.SetActive(true);
                    image_ammo2.SetActive(true);
                    image_ammo3.SetActive(true);
                    image_ammo4.SetActive(true);
                    image_ammo5.SetActive(false);
                    image_ammo6.SetActive(false);
                    break;
            case 3: image_ammo1.SetActive(true);
                    image_ammo2.SetActive(true);
                    image_ammo3.SetActive(true);
                    image_ammo4.SetActive(false);
                    image_ammo5.SetActive(false);
                    image_ammo6.SetActive(false);
                    break;
            case 2: image_ammo1.SetActive(true);
                    image_ammo2.SetActive(true);
                    image_ammo3.SetActive(false);
                    image_ammo4.SetActive(false);
                    image_ammo5.SetActive(false);
                    image_ammo6.SetActive(false);
                    break;
            case 1: image_ammo1.SetActive(true);
                    image_ammo2.SetActive(false);
                    image_ammo3.SetActive(false);
                    image_ammo4.SetActive(false);
                    image_ammo5.SetActive(false);
                    image_ammo6.SetActive(false);
                    break;
            case 0: image_ammo1.SetActive(false);
                    image_ammo2.SetActive(false);
                    image_ammo3.SetActive(false);
                    image_ammo4.SetActive(false);
                    image_ammo5.SetActive(false);
                    image_ammo6.SetActive(false);
                    break;
            
        }
    }
}
