using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameManager gm_instance;

    public int score;
    public int combo_stack;


    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (gm_instance != null && gm_instance != this)
        {
            Destroy(this);
        }
        else
        {
            gm_instance = this;
        }
    }
    
}
