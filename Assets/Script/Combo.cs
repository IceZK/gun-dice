using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Combo : MonoBehaviour
{
    public TextMeshProUGUI text_combo;
    public TextMeshProUGUI text_combo_trigger;

    public float timeElapse = 0;
    public bool triggerCombo = false;
    void Start()
    {
        text_combo.text = "";
        text_combo_trigger.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.gm_instance.combo_stack >= 10) text_combo.text = GameManager.gm_instance.combo_stack + " COMBO!!!";
        if (GameManager.gm_instance.combo_stack <= 0) text_combo.text = "";
        if (GameManager.gm_instance.combo_stack % 25 == 0 && GameManager.gm_instance.combo_stack > 0)
        {
            text_combo_trigger.text = Mathf.FloorToInt(GameManager.gm_instance.combo_stack / 25) * 25 + " COMBO!!!";
            triggerCombo = true;
        }
        if (triggerCombo)
        {
            timeElapse += Time.deltaTime;
            if (timeElapse >= 2)
            {
                text_combo_trigger.text = "";
                timeElapse = 0;
                triggerCombo= false;
            }
        }
    }
}
