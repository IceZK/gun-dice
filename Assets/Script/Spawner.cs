using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // Start is called before the first frame update
    public AiMovement Ai;
    public AiMovement Ai2;
    public GameObject enemy;
    public GameObject enemy2;
    public GameObject[] Spawnpoint;
    public float spawn_time = 1;
    public float reset_time = 0.5f;
    public float GameTime;
    public int i = 1;
    public int enemy_random;


    void Start()
    {
        Ai = enemy.GetComponent<AiMovement>();
        Ai2 = enemy2.GetComponent<AiMovement>();
        Ai.speed = 10;
        Ai2.speed = 10;
    }

    // Update is called once per frame
    void Update()
    {
        spawn_time -= Time.deltaTime;
       
        
        if (spawn_time <= 0)
        {
            enemy_random = Random.Range(1, 3);

             if (enemy_random == 1) 
            {
                Instantiate(enemy, Spawnpoint[i].transform.position, Quaternion.identity);
            }
            else if(enemy_random == 2)
            {
                Instantiate(enemy2, Spawnpoint[i].transform.position, Quaternion.identity);
            }



            spawn_time = reset_time;
            i = Random.Range(0, 2);
            if (GameManager.gm_instance.score % 25 == 0 && GameManager.gm_instance.score > 0)
            {
                if(reset_time <= 0)
                {
                    reset_time = 0.0f;
                }
                else if(reset_time > 0)
                {
                    reset_time -= 0.001f;
                }
                Ai.speed += 1;
                Ai2.speed += 1;
            }
        }
        
        
        
    }
}
