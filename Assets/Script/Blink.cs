using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Blink : MonoBehaviour
{
    // Start is called before the first frame update
    public Color color1;
    public Color color2;
    public TextMeshProUGUI text;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FlashingText();
    }

    public void FlashingText()
    {
        text.color = Color.Lerp(color1, color2, Mathf.PingPong(Time.time,1));
    }
}
